#!/bin/sh

if [ "$DATABASE" = "postgres" ]
then
    echo "Waiting for postgres..."

    while ! nc -z $SQL_HOST $SQL_PORT; do
      sleep 0.1
    done

    echo "PostgreSQL started"
fi

python manage.py migrate

if [ -z ${ADMIN_USERNAME+x} ] || [ -z ${ADMIN_PASSWORD+x} ];
then
  echo "Missing admin username and password";
else
  echo "Create sdc user"
  python ./manage.py create_user --username $ADMIN_USERNAME --password $ADMIN_PASSWORD --staff --superuser
fi

if [ -z ${SDC_USERNAME+x} ] || [ -z ${SDC_PASSWORD+x} ];
then
  echo "Missing sdc username and password";
else
  echo "Create sdc user"
  python ./manage.py create_user --username $SDC_USERNAME --password $SDC_PASSWORD --staff
fi

if [ -z ${CSI_USERNAME+x} ] || [ -z ${CSI_PASSWORD+x} ];
then
  echo "Missing csi username and password";
else
  echo "Create csi user"
  python ./manage.py create_user --username $CSI_USERNAME --password $CSI_PASSWORD
fi


exec "$@"