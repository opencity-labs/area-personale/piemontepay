import xmltodict

namespaces = {
    'http://schemas.xmlsoap.org/soap/envelope/': None,
    'http://www.csi.it/epay/epaywso/': None,
    'http://www.csi.it/epay/epaywso/types': None,
    'http://www.csi.it/epay/epaywso/epaywso2enti/types': None
}


def xml_to_dict(xml):
    return xmltodict.parse(xml, process_namespaces=True, namespaces=namespaces)['Envelope']['Body']
