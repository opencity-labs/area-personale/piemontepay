from .models import PosizioneDebitoria, ListaDiCarico
from rest_framework import serializers


# Serializers define the API representation.

class ListaDiCaricoSerializer(serializers.ModelSerializer):
    class Meta:
        model = ListaDiCarico
        fields = '__all__'


class PosizioneDebitoriaSerializer(serializers.ModelSerializer):
    class Meta:
        model = PosizioneDebitoria
        fields = '__all__'
