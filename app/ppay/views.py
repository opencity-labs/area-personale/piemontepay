import json

from django.db.models import Model
from django.http import HttpResponse, HttpResponseNotAllowed
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from .models import PosizioneDebitoria, ListaDiCarico
from .utils import xml_to_dict
# Create your views here.
from rest_framework import viewsets, permissions, generics

from .serializers import PosizioneDebitoriaSerializer


def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")


class PosizioneDebitoriaViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = PosizioneDebitoria.objects.all()
    serializer_class = PosizioneDebitoriaSerializer
    permission_classes = [permissions.IsAuthenticated, permissions.IsAdminUser]


@method_decorator(csrf_exempt, name='dispatch')
class EsitoInserimento(View):
    http_method_names = ["post"]

    def post(self, request, *args, **kwargs):
        response = HttpResponse(content_type='application/json')
        if request.content_type != 'application/xml':
            response.status_code = 415
            return response

        esito_inserimento_response = xml_to_dict(request.body)
        print(json.dumps(esito_inserimento_response, indent=2))
        testata = esito_inserimento_response["EsitoInserimentoListaDiCaricoRequest"]["TestataEsito"]
        esito_inserimento = esito_inserimento_response["EsitoInserimentoListaDiCaricoRequest"]["EsitoInserimento"]
        try:
            lista_di_carico = ListaDiCarico.objects.get(
                id_messaggio=testata["IdMessaggio"],
                ente_creditore__cf=testata["CFEnteCreditore"],
                codice_versamento=testata["CodiceVersamento"]
            )
        except ListaDiCarico.DoesNotExist:
            response.status_code = 404
            return response

        if isinstance(esito_inserimento["ElencoPosizioniDebitorieInserite"]["PosizioneDebitoriaInserita"], dict):
            esito_inserimento["ElencoPosizioniDebitorieInserite"]["PosizioneDebitoriaInserita"] = [
                esito_inserimento["ElencoPosizioniDebitorieInserite"]["PosizioneDebitoriaInserita"]
            ]

        error = False
        for posizione_inserita in esito_inserimento["ElencoPosizioniDebitorieInserite"][
            "PosizioneDebitoriaInserita"]:
            try:
                posizione_debitoria = PosizioneDebitoria.objects.get(
                    id_posizione_debitoria=posizione_inserita["IdPosizioneDebitoria"],
                    lista_di_carico=lista_di_carico
                )
            except PosizioneDebitoria.DoesNotExist:
                error = True
            else:
                posizione_debitoria.codice_esito = posizione_inserita["CodiceEsito"]
                if "IUV" in posizione_inserita:
                    posizione_debitoria.iuv = posizione_inserita["IUV"]
                if "CodiceAvviso" in posizione_inserita:
                    posizione_debitoria.codice_avviso = posizione_inserita["CodiceAvviso"]
                if "DescrizioneEsito" in posizione_inserita:
                    posizione_debitoria.descrizione_esito = posizione_inserita["DescrizioneEsito"]

                posizione_debitoria.save()

        if error:
            response.status_code = 404
        return response
